# JECS
本包说明：
* 本包为一款基于easyAi的JAVA高性能，低成本，轻量级智能客服助手，可进行用户沟通的自动抓取语义响应与抓取需求关键词服务
* 为电商类，咨询类，平台类服务应用(web/小程序/APP/JAVA)，提供高性能的自动智能客服支持
* 本包对中文输入语句，对输入语句的类别进行分类，关键词抓取，词延伸，以及集成智能客服功能在逐渐扩展中
## 详细视频教程地址：
* 视频教程地址：https://www.bilibili.com/video/BV1W7411J7zr?p=3&vd_source=20cf39c973b43e60c3bdbe8d47dc9e71
### API 说明:
``` java
    com.jecs.config.Config
    //配置文件的模型文件保存地址，用户可自定义输入，若该地址无文件则自动进入训练模块
    public static final String SentenceModelUrl = "E:\\model\\sentence5.json";//语句生成model本地url
    public static final String KeyWordModelUrl = "E:\\model\\keyWord3.json";//关键词查找模型本地url
    //数据库表映射层，用户可自定义表结构与sql语句
    com.jecs.mapper.SqlMapper
    //
    com.jecs.entity.Sentence 训练模板类,用户自行根据该实体类定义表结构
    private String word;//语句
    private int second;//语句类别
    private String noun;//该关键词
```
### 演示数据脚本
演示语句：sentence.sql
关键词模型文件:keyWord.json
语句延伸模型文件:sentence6.json
### 依赖
服务依赖：
* springboot
* easyAi：https://gitee.com/ldp_dpsmax/easyAi
### 常见抛错
* 表中的模板语句量少于101：Template statement must be greater than 101
* 语句类别id不可以等于0：type id Cannot be 0

