package com.jecs.tools;

import com.alibaba.fastjson.JSONObject;
import com.jecs.bean.BeanMangerOnly;
import com.jecs.config.Config;
import com.jecs.entity.KeyWordModelMapping;
import com.jecs.entity.MyWordModel;
import com.jecs.entity.Sentence;
import org.wlld.entity.KeyWordForSentence;
import org.wlld.naturalLanguage.languageCreator.CatchKeyWord;
import org.wlld.naturalLanguage.languageCreator.CreatorSentenceModel;
import org.wlld.naturalLanguage.languageCreator.KeyWordModel;
import org.wlld.naturalLanguage.languageCreator.SentenceCreator;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tools {
    private CreatorSentenceModel readCreatorSentenceModel(BeanMangerOnly beanMangerOnly, List<String> sentenceList) throws Exception {
        File file = new File(Config.SentenceModelUrl);//创建文件
        CreatorSentenceModel model = null;
        if (file.exists()) {//模型文件存在
            model = JSONObject.parseObject(readPaper(file), CreatorSentenceModel.class);
        } else {//模型文件不存在进行训练
            SentenceCreator creator = beanMangerOnly.sentenceCreator();
            creator.initFirst(sentenceList, beanMangerOnly.temple());
            creator.study();//非常耗时的过程
            String myModel = JSONObject.toJSONString(creator.getModel());
            writeModel(myModel, Config.SentenceModelUrl);//训练结束保存模型文件
        }
        return model;
    }

    private void keyWord(BeanMangerOnly beanMangerOnly, List<Sentence> sentences) throws IOException {//处理关键词
        File file = new File(Config.KeyWordModelUrl); //创建文件
        Map<Integer, CatchKeyWord> catchKeyWordMap = beanMangerOnly.catchKeyWord();
        if (!file.exists()) {//重新学习
            List<KeyWordModelMapping> keyWordModelMappings = new ArrayList<>();
            Map<Integer, List<Sentence>> sentenceMap = new HashMap<>();
            for (Sentence sentence : sentences) {
                int key = sentence.getSecond();//类别
                String word = sentence.getWord();
                String keyWord = sentence.getNoun();//关键词
                if (word != null && word.length() > 0 && keyWord != null && keyWord.length() > 0) {
                    if (sentenceMap.containsKey(key)) {
                        sentenceMap.get(key).add(sentence);
                    } else {
                        List<Sentence> sentenceList = new ArrayList<>();
                        sentenceList.add(sentence);
                        sentenceMap.put(key, sentenceList);
                    }
                }
            }
            for (Map.Entry<Integer, List<Sentence>> entry : sentenceMap.entrySet()) {
                List<Sentence> sentenceList = entry.getValue();
                int key = entry.getKey();
                List<KeyWordForSentence> keyWordForSentenceList = new ArrayList<>();
                CatchKeyWord catchKeyWord = new CatchKeyWord();
                catchKeyWordMap.put(key, catchKeyWord);//TODO 吃内存
                System.out.println("key:" + key);
                for (Sentence sentence : sentenceList) {
                    KeyWordForSentence keyWordForSentence = new KeyWordForSentence();
                    keyWordForSentence.setSentence(sentence.getWord());
                    keyWordForSentence.setKeyWord(sentence.getNoun());
                    keyWordForSentenceList.add(keyWordForSentence);
                }
                catchKeyWord.study(keyWordForSentenceList);//耗时的过程
                KeyWordModelMapping keyWordModelMapping = new KeyWordModelMapping();
                keyWordModelMapping.setKey(key);
                keyWordModelMapping.setKeyWordModel(catchKeyWord.getModel());
                keyWordModelMappings.add(keyWordModelMapping);
            }
            MyWordModel model = new MyWordModel();
            model.setKeyWordModelMappings(keyWordModelMappings);
            //模型写出
            writeModel(JSONObject.toJSONString(model), Config.KeyWordModelUrl);
        } else {//TODO 读取模型
            List<KeyWordModelMapping> keyWordModels = JSONObject.parseObject(readPaper(file), MyWordModel.class).getKeyWordModelMappings();
            for (KeyWordModelMapping keyWordModelMapping : keyWordModels) {
                int key = keyWordModelMapping.getKey();
                CatchKeyWord catchKeyWord = new CatchKeyWord();
                catchKeyWordMap.put(key, catchKeyWord);
                catchKeyWord.insertModel(keyWordModelMapping.getKeyWordModel());
            }
        }
    }

    public void initSemantics(BeanMangerOnly beanMangerOnly, List<Sentence> sentences) throws Exception {
        if (sentences.size() > 0) {
            Map<Integer, List<String>> model = new HashMap<>();//语义识别
            List<String> sentenceList = new ArrayList<>();//语句延伸 与 关键词抓取
            for (Sentence sentence : sentences) {
                int type = sentence.getSecond();
                if (type == 0) {
                    throw new Exception("type id Cannot be 0");
                }
                String word = sentence.getWord();
                sentenceList.add(word);
                if (model.containsKey(type)) {
                    model.get(type).add(word);
                } else {
                    List<String> words = new ArrayList<>();
                    words.add(word);
                    model.put(type, words);
                }
            }
            beanMangerOnly.tokenizer().start(model);//拆词及rf学习
            keyWord(beanMangerOnly, sentences);//读取关键词模型
            CreatorSentenceModel creatorModel = readCreatorSentenceModel(beanMangerOnly, sentenceList);
            if (creatorModel != null) {//有模型
                beanMangerOnly.sentenceCreator().initModel(beanMangerOnly.temple(), creatorModel);//生成词句模型
            }
        } else {
            throw new Exception("Template statement must be greater than 101");
        }

    }

    private void writeModel(String model, String url) throws IOException {//写出模型与 激活参数
        BufferedWriter out = new BufferedWriter(new FileWriter(url));
        out.write(model);
        out.close();
    }

    private String readPaper(File file) {
        InputStream read = null;
        String context = null;
        try {
            //一次读一个字符
            read = new FileInputStream(file);
            byte[] bt = new byte[read.available()];
            read.read(bt);
            context = new String(bt, "UTF-8");
            read.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (read != null) {
                try {
                    read.close(); //确保关闭
                } catch (IOException el) {
                }
            }
        }

        return context;
    }
}
