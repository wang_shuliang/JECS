package com.jecs.business;


import com.jecs.bean.BeanMangerOnly;
import com.jecs.config.Config;
import com.jecs.constant.ErrorCode;
import com.jecs.entity.Response;
import com.jecs.entity.Shop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wlld.naturalLanguage.Talk;
import org.wlld.naturalLanguage.languageCreator.CatchKeyWord;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@Service
public class Business {
    @Autowired
    private BeanMangerOnly beanMangerOnly;

    public Response talk(String word) throws Exception {//对话返回分类
        Response response = new Response();
        if (word != null && word.replace(" ", "").length() > 1) {
            String[] sens = word.replace(" ", "").split("，|。|？|！|；|、|：");
            Map<Integer, CatchKeyWord> catchKeyWordMap = beanMangerOnly.catchKeyWord();
            Shop shop = new Shop();//协议
            Talk talk = new Talk(beanMangerOnly.temple());//语义识别类
            int type = 0;
            Set<String> keyWords = null;
            for (int i = 0; i < sens.length; i++) {
                keyWords = null;
                String sen = sens[i];
                List<String> words = talk.getSplitWord(sen).get(0);//对该词进行关键字提取
                if (words.size() < Config.MinWordLength) {//拆词数量小于阈值，则对语句进行补全
                    String fillName = beanMangerOnly.sentenceCreator().fill(sen, talk);//补全词 三百-500毫秒
                    type = talk.talk(fillName).get(0);//二级分类
                } else {//5毫秒
                    type = talk.talk(sen).get(0);//二级分类
                }
                keyWords = catchKeyWordMap.get(type).getKeyWord(sen);
                if (keyWords.size() == 0) {
                    shop.setKeyWord(false);
                } else {
                    shop.setKeyWord(true);
                }
                if (type > 0) {
                    break;
                }
            }
            shop.setSecondType(type);
            shop.setKeyWords(keyWords);
            response.setData(shop);
        } else {
            response.setError(ErrorCode.WordIsNull.getError());
            response.setErrorMessage(ErrorCode.WordIsNull.getErrorMessage());
        }
        return response;
    }

}
