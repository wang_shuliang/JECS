package com.jecs.controller;

import com.jecs.business.Business;
import com.jecs.entity.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@RestController
@RequestMapping("/ai")
@Api(tags = "语言服务")
public class Controller {
    @Autowired
    private Business business;

    @RequestMapping(value = "/talk", method = RequestMethod.POST)//
    @ApiOperation("对话")
    @ApiImplicitParam(name = "sentence", value = "用户对话语句", defaultValue = "帮我修理钢琴", required = true)
    public Response talk(@RequestBody String sentence) throws Exception {
        return business.talk(sentence);
    }

}
