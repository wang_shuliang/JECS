package com.jecs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Set;


/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@ApiModel
public class Shop {
    @ApiModelProperty(value = "二级分类id", example = "2")
    private int secondType;
    @ApiModelProperty(value = "关键词集合")
    private Set<String> keyWords;//关键词
    @ApiModelProperty(value = "是否查找到关键词", example = "true")
    private boolean isKeyWord;

    public Set<String> getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(Set<String> keyWords) {
        this.keyWords = keyWords;
    }

    public boolean isKeyWord() {
        return isKeyWord;
    }

    public void setKeyWord(boolean keyWord) {
        isKeyWord = keyWord;
    }



    public int getSecondType() {
        return secondType;
    }

    public void setSecondType(int secondType) {
        this.secondType = secondType;
    }
}
