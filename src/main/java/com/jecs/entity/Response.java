package com.jecs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@ApiModel
public class Response {
    @ApiModelProperty(value = "错误码", example = "1")
    private int error;
    @ApiModelProperty(value = "错误信息", example = "通讯异常")
    private String errorMessage;
    @ApiModelProperty(value = "返回消息体")
    private Object data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
