package com.jecs.entity;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
public class Sentence {
    private String word;//语句
    private int second;//语句类别
    private String noun;//该关键词

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public String getNoun() {
        return noun;
    }

    public void setNoun(String noun) {
        this.noun = noun;
    }

    public String getMyTalk() {
        return word;
    }

    public void setMyTalk(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
