package com.jecs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@ApiModel
public class KeyWord {
    @ApiModelProperty(value = "关键字字符", example = "开锁")
    private String keyword;
    @ApiModelProperty(value = "所属的二级分类id", example = "2")
    private int second;
    @ApiModelProperty(value = "三级分类id", example = "3")
    private int id;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
