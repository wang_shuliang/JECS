package com.jecs;


import com.jecs.bean.BeanManger;
import com.jecs.bean.BeanMangerOnly;
import com.jecs.entity.Sentence;
import com.jecs.mapper.SqlMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import java.util.List;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@SpringBootApplication
public class Service {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(Service.class, args);
        init(applicationContext);
    }

    private static void init(ConfigurableApplicationContext applicationContext) throws Exception {//初始化启动配置
        SqlMapper sql = applicationContext.getBean(SqlMapper.class);
        List<Sentence> sentences = sql.getModel();//数据库模板，用户可自己修改数据库信息
        BeanMangerOnly beanMangerOnly = applicationContext.getBean(BeanMangerOnly.class);
        applicationContext.getBean(BeanManger.class).tools().initSemantics(beanMangerOnly, sentences);
        System.out.println("完成初始化");
    }
}
