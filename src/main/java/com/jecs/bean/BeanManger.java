package com.jecs.bean;

import com.jecs.entity.Response;
import com.jecs.entity.Shop;
import com.jecs.tools.Tools;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@Configuration
@Scope("prototype")
public class BeanManger {
    @Bean
    public Response response() {
        return new Response();
    }

    @Bean
    public Shop shop() {
        return new Shop();
    }

    @Bean
    public Tools tools() {
        return new Tools();
    }
}
