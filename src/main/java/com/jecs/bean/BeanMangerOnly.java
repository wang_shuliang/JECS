package com.jecs.bean;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.wlld.naturalLanguage.Tokenizer;
import org.wlld.naturalLanguage.WordTemple;
import org.wlld.naturalLanguage.languageCreator.CatchKeyWord;
import org.wlld.naturalLanguage.languageCreator.SentenceCreator;

import java.util.HashMap;
import java.util.Map;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description
 */
@Configuration
public class BeanMangerOnly {//需要单例的类

    @Bean
    public WordTemple temple() {//模板配置类
        WordTemple wordTemple = new WordTemple();
        wordTemple.setTreeNub(7);//设置森林当中树木的数量
        wordTemple.setGarbageTh(0.5);//设置可识别阈值（错误率）
        wordTemple.setTrustPunishment(0.5);//信任惩罚 惩罚连乘
        wordTemple.setTrustTh(0.1);//信任阈值  阈值乘树的个数
        wordTemple.setSplitWord(false);//只做分词
        return wordTemple;
    }

    @Bean//语义训练类
    public Tokenizer tokenizer() {//学习类
        return new Tokenizer(temple());
    }

    @Bean//语句延伸类
    public SentenceCreator sentenceCreator() {//自动语句延伸类
        return new SentenceCreator();
    }

    @Bean//关键词抓取类
    public Map<Integer,CatchKeyWord> catchKeyWord() {
        return new HashMap<>();
    }
}
