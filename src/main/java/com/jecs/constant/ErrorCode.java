package com.jecs.constant;

public enum ErrorCode {
    OK("正常", 0),
    WordIsNull("说话语句长度小于2", 1);
    private String errorMessage;
    private int error;

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getError() {
        return error;
    }

    ErrorCode(String errorMessage, int error) {
        this.errorMessage = errorMessage;
        this.error = error;
    }

}
