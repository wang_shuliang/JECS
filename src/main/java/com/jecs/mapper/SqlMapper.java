package com.jecs.mapper;

import com.jecs.entity.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SqlMapper {
    //展示分类的所有结构关系
    //获取当前库里全部模板//where second>5 and second <> 19 and second <> 27 and second<>28
    @Select("select * from sentence where second>5 and second <> 19 and second <> 27 and second<>28")
    List<Sentence> getModel();
}